import { Component, OnInit } from '@angular/core';
import 'hammerjs';
import 'web-animations-js';
import * as Muuri from 'muuri';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Dashboard';

  public itemContainers: Element[];
  public columnGrids: any[];
  public boardGrid: any;

  ngOnInit(): void {
    this.itemContainers = [].slice.call(document.querySelectorAll('.dragable-column'));
    this.columnGrids = [];
    this.itemContainers.forEach((container) => {
    const grid = new Muuri(container, {
      items: '.card',
      layoutDuration: 400,
      layoutEasing: 'ease',
      dragEnabled: true,
      dragSort: () => {
        return this.columnGrids;
      },
      dragStartPredicate: {
        handle: '.card-head'
      },
      dragHammerSettings: {
        touchAction: 'auto',
      },
      dragSortInterval: 0,
      dragContainer: document.body,
      dragReleaseDuration: 400,
      dragReleaseEasing: 'ease',
    })
    .on('dragStart', (item) => {
      item.getElement().style.width = item.getWidth() + 'px';
      item.getElement().style.height = item.getHeight() + 'px';
    })
    .on('dragReleaseEnd', (item) => {
      item.getElement().style.width = '';
      item.getElement().style.height = '';
      this.columnGrids.forEach(x => {
          x.refreshItems();
        }
      );
    })
    .on('layoutStart', () => {
      this.boardGrid.refreshItems().layout();
    });
    this.columnGrids.push(grid);
    });
    this.boardGrid = new Muuri('.dashboard', {
      layoutDuration: 400,
      layoutEasing: 'ease',
      dragEnabled: false,
      dragSortInterval: 0,
      dragReleaseDuration: 400,
      dragReleaseEasing: 'ease'
    });
  }
}
